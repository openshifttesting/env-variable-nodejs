var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', learnerName: process.env.LEARNERNAME, labName : process.env.LABNAME});
});


module.exports = router;
